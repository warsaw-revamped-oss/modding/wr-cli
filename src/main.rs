use clap::Parser;

use crate::args::{CommandHandler, Commands, WrCli};
use crate::commands::create::CreateHandler;
use crate::commands::dependency::DependencyHandler;
use crate::commands::publish::PublishHandler;
use crate::commands::sign::SignHandler;

mod args;
mod commands;

fn main() {
    let args = WrCli::parse();

    // dispatch the command
    let res = match args.command {
        Commands::Create(create_args) => CreateHandler::handle(create_args),
        Commands::Dependency(dependency_args) => DependencyHandler::handle(dependency_args),
        Commands::Publish(publish_args) => PublishHandler::handle(publish_args),
        Commands::Sign(sign_args) => SignHandler::handle(sign_args),
    };

    if let Err(err) = res {
        eprintln!("Error: {err}")
    }
}
