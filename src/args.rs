use std::error::Error;

use clap::{Parser, Subcommand};

use crate::commands::create::CreateArgs;
use crate::commands::dependency::DependencyCommand;
use crate::commands::publish::PublishArgs;
use crate::commands::sign::SignArgs;

#[derive(Debug, Parser)]
#[command(name = "wr-cli")]
pub struct WrCli {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Subcommand)]
#[command(about = "The command-line interface for Warsaw Revamped Script Management.")]
pub enum Commands {
    Create(CreateArgs),
    #[command(subcommand)]
    Dependency(DependencyCommand),
    Publish(PublishArgs),
    Sign(SignArgs),
}

/// A handler that... handles commands.
pub trait CommandHandler {
    type Command;
    type Error: Error + 'static;

    fn handle(command: Self::Command) -> Result<(), Box<dyn Error>> {
        Self::handle_impl(command).map_err(|err| err.into())
    }

    /// Handle the command.
    fn handle_impl(command: Self::Command) -> Result<(), Self::Error>;
}
