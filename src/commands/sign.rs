use crate::args::CommandHandler;
use clap::Parser;
use ed25519_dalek::pkcs8::{DecodePrivateKey, EncodePrivateKey, EncodePublicKey};
use ed25519_dalek::{Signer, SigningKey};
use rand::rngs::OsRng;
use std::path::{Path, PathBuf};
use std::{fs, io};

#[derive(Debug, Parser)]
#[command(about = "Utilities to sign packages.")]
pub struct SignArgs {
    #[arg(
        short,
        long,
        help = "The path to the key file.",
        default_value = "wr-sign.der"
    )]
    pub key_file: PathBuf,
    #[arg(short, long, help = "Generate a new signing key.")]
    pub generate: bool,
    #[arg(
        short,
        long,
        help = "The path to the output directory for key generation."
    )]
    pub out: Option<PathBuf>,
    #[arg(
        short,
        long,
        help = "The path to the file to sign. The signature will be generated as <file_name>.sig"
    )]
    pub target: Option<PathBuf>,
}

pub struct SignHandler {}

impl SignHandler {
    /// Sign a file.
    pub fn sign_file<P1: AsRef<Path>, P2: AsRef<Path>>(key_file: P1, target: P2) -> io::Result<()> {
        // read in the key file
        let signing_key =
            SigningKey::read_pkcs8_der_file(key_file).expect("Failed to read private key");
        let target_contents = fs::read_to_string(target.as_ref())?;

        let sig = signing_key.sign(target_contents.as_bytes());
        let sig_path = target.as_ref().with_extension("sig");
        fs::write(sig_path, sig.to_bytes())?;

        Ok(())
    }

    /// Generate a key used to sign.
    fn generate_signing_key<P: AsRef<Path>>(out_path: P) -> io::Result<()> {
        let out_path = out_path.as_ref();
        assert!(out_path.is_dir());

        // generate a key
        let new_key = SigningKey::generate(&mut OsRng);
        new_key
            .write_pkcs8_der_file(out_path.join("wr-sign.der"))
            .expect("Failed to write private key");
        new_key
            .verifying_key()
            .write_public_key_der_file(out_path.join("wr-sign.pub"))
            .expect("Failed to write public key");

        Ok(())
    }
}

impl CommandHandler for SignHandler {
    type Command = SignArgs;
    type Error = io::Error;

    fn handle_impl(command: Self::Command) -> Result<(), Self::Error> {
        // generate the key pair if requested
        if command.generate {
            Self::generate_signing_key(
                command
                    .out
                    .as_ref()
                    .expect("Expected `--out` to be specified for a new key!"),
            )?;
            return Ok(());
        }

        // sign a file!
        Self::sign_file(
            &command.key_file,
            command
                .target
                .expect("Expected `--target` to be specified for the signature!"),
        )?;

        Ok(())
    }
}
