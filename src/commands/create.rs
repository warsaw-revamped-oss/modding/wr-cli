use std::io;

use clap::Parser;

use crate::args::CommandHandler;

#[derive(Debug, Parser)]
#[command(about = "Create a new WR script project.")]
pub struct CreateArgs {
    #[arg(long, short, help = "The name of the project.")]
    pub name: String,
}

pub struct CreateHandler {}

impl CommandHandler for CreateHandler {
    type Command = CreateArgs;
    type Error = io::Error;

    fn handle_impl(_command: Self::Command) -> Result<(), Self::Error> {
        todo!()
    }
}
