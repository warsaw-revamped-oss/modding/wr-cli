use std::fs::File;
use std::io::{Cursor, Read};
use std::path::{Path, PathBuf};
use std::{fs, io};

use clap::Parser;
use ed25519_dalek::ed25519::SignatureBytes;
use ed25519_dalek::pkcs8::DecodePrivateKey;
use ed25519_dalek::{Signer, SigningKey};
use thiserror::Error;
use walkdir::WalkDir;
use wr_api_manifest::hash_db::HashDB;
use wr_api_manifest::v1::{WR_HASH_PATH, WR_HASH_SIG_PATH, WR_MANIFEST_PATH};
use wr_api_manifest::{v1, Manifest};
use zip::result::ZipError;
use zip::write::SimpleFileOptions;
use zip::ZipWriter;

use crate::args::CommandHandler;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Error deserializing manifest: {0}")]
    Deserialize(#[from] toml::de::Error),
    #[error("IO Error: {0}")]
    Io(#[from] io::Error),
    #[error("Error serializing Hash DB: {0}")]
    Serialize(#[from] toml::ser::Error),
    #[error("Failed to write to zip: {0}")]
    Zip(#[from] ZipError),
}

#[derive(Debug, Parser)]
#[command(about = "Publish your WR script to the marketplace.")]
pub struct PublishArgs {
    #[arg(
        short,
        long,
        help = "The path to the key file.",
        default_value = "wr-sign.der"
    )]
    pub key_file: PathBuf,
    #[arg(
        short, long,
        help = "The root directory of the script (default working directory)",
        default_value_os_t = std::env::current_dir().unwrap()
    )]
    pub root: PathBuf,
}

pub struct PublishHandler {}

impl PublishHandler {
    /// Reads all allowed files in the root.
    fn allowed_files<P: AsRef<Path>>(root: P) -> impl Iterator<Item = PathBuf> {
        WalkDir::new(root)
            .into_iter()
            .filter_map(Result::ok)
            .filter(|entry| v1::filter_file(entry.path()))
            .map(|entry| entry.path().to_owned())
    }

    /// Reads the WR API manifest from the local directory.
    fn read_manifest<P: AsRef<Path>>(root: P) -> Result<Manifest, Error> {
        let mut manifest_file = File::open(root.as_ref().join(WR_MANIFEST_PATH))?;
        let mut manifest_str = String::new();
        manifest_file.read_to_string(&mut manifest_str)?;

        Ok(toml::from_str(&manifest_str)?)
    }

    /// Sign a file.
    pub fn sign<P: AsRef<Path>, C: AsRef<[u8]>>(
        key_file: P,
        contents: C,
    ) -> io::Result<SignatureBytes> {
        // read in the key file
        let signing_key =
            SigningKey::read_pkcs8_der_file(key_file).expect("Failed to read private key");
        let sig = signing_key.sign(contents.as_ref());

        Ok(sig.to_bytes())
    }
}

impl CommandHandler for PublishHandler {
    type Command = PublishArgs;
    type Error = Error;

    fn handle_impl(command: Self::Command) -> Result<(), Self::Error> {
        let root = command.root;

        // read the manifest and ensure that everything is valid
        let details = Self::read_manifest(&root)?.script;

        // open the ZIP file
        let mut zip = ZipWriter::new(File::create(
            root.join(format!("{}-{}.zip", details.name, details.version)),
        )?);

        // iterate through allowed files and collect them into a hash database
        let mut hash_db = HashDB::default();
        for allowed_file in Self::allowed_files(&root) {
            let file_path = allowed_file
                .strip_prefix(&root)
                .expect("Failed to strip prefix")
                .to_str()
                .expect("File was not valid");
            // read the contents
            let contents = fs::read(&allowed_file)?;
            // insert it into the ZIP file
            zip.start_file(file_path, SimpleFileOptions::default())?;
            io::copy(&mut Cursor::new(&contents), &mut zip)?;
            // insert it into the hash DB
            hash_db.insert_entry(file_path, Cursor::new(&contents))?;
        }

        // insert the hash database
        let hash_db_str = toml::to_string(&hash_db)?;
        zip.start_file(WR_HASH_PATH, SimpleFileOptions::default())?;
        io::copy(&mut Cursor::new(&hash_db_str), &mut zip)?;

        // sign the hash DB
        let sig = Self::sign(command.key_file, hash_db_str)?;
        // write the signature
        zip.start_file(WR_HASH_SIG_PATH, SimpleFileOptions::default())?;
        io::copy(&mut Cursor::new(sig), &mut zip)?;

        // finish teh zip file
        zip.finish()?;

        Ok(())
    }
}
