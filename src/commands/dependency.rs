use std::io;

use clap::{Parser, Subcommand};
use semver::Version;

use crate::args::CommandHandler;

#[derive(Debug, Subcommand)]
#[command(name = "dependency", about = "Tools to manipulate dependencies.")]
pub enum DependencyCommand {
    #[command(about = "Add a new dependency to the active project.")]
    Add(DependencyAddArgs),
    #[command(about = "Update all of the included dependencies.")]
    Update,
}

#[derive(Debug, Parser)]
pub struct DependencyAddArgs {
    #[arg(long, short, help = "The name of the dependency to add.")]
    pub name: String,
    #[arg(long, short, help = "The version of the dependency to add.")]
    pub version: Version,
}

pub struct DependencyHandler {}

impl DependencyHandler {
    fn handle_add(_add_args: DependencyAddArgs) {
        todo!()
    }

    fn handle_update() {
        todo!()
    }
}

impl CommandHandler for DependencyHandler {
    type Command = DependencyCommand;
    type Error = io::Error;

    fn handle_impl(command: Self::Command) -> Result<(), Self::Error> {
        match command {
            DependencyCommand::Add(add_args) => Self::handle_add(add_args),
            DependencyCommand::Update => Self::handle_update(),
        }

        Ok(())
    }
}
